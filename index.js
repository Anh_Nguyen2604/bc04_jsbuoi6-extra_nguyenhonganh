

function isPrime(num) {
    
    for (var i = 2; i <= Math.sqrt(num); i++) {
      if (num % i == 0) {
        return 0;
      }
    }
    return 1;
  };
  
  document.getElementById("nguyenTo").onclick = function () {
  
    var soNguyen = document.getElementById("txt-so-nguyen-to").value * 1;
  
    var numPrime = "";
  
    for (var i = 2; i <= soNguyen; i++) {
      if (isPrime(i) == 1) {
        numPrime += i + " ";
      }
    }
  
    document.getElementById("result").innerHTML = numPrime;
  };
  
